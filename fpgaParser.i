%module fpgaParser
%include "stdint.i"
%{
#define SWIG_FILE_WITH_INIT
 #include "fpgaParser.h"
%}

void startThread();
void stopThread();
void sendData(char *dataBuf, uint32_t bufLen);

