//#include <iostream>
#include "ftd2xx.h"
#include <windows.h>
//#include <thread>

#include <stdio.h>
//#include <conio.h>
#include <process.h>
#include <stdbool.h>
#include <stdint.h>
//using namespace std;

void threadUsb(void *);
void startThread(void);
void stopThread(void);

bool bRunning=true;
uint8_t recvBuf[65536];

HANDLE hThread = NULL;

int main()
{
    char inStr[256];
    //std::string str;
    startThread();
    while(true){
        fgets(inStr, sizeof(inStr), stdin);
        printf(inStr);
        //char a;
//        std::cin >> str;
//        if(str=="start"){
//            std::cout << "start thread" << std::endl;
//            startThread();
//        }
//        else if(str=="stop"){
//            std::cout << "stop thread"<< std::endl;
//            stopThread();

//        }

    }


    return 0;
}

void startThread()
{
    printf("start thread");
    if(hThread == NULL){
        bRunning=true;
        hThread = (HANDLE)_beginthread( threadUsb, 0, NULL );
    }
}

void stopThread()
{
    printf("stop thread");
    bRunning = false;
    WaitForSingleObject( hThread, INFINITE );
    hThread = NULL;
}


typedef enum{
    searchForKey,
    searchForFrameHeaderFWOrKey,
    searchForFrameHeaderFW,
    searchForFrameHeaderSW,
    searchForPacketHeaderFW,
    searchForPacketHeaderSW,
    searchForPacketEpilogFW,
    searchForPacketEpilogSW,
    searchForFrameEpilogFW,
    searchForFrameEpilogSW,
    searchForAddr,
    searchForData,
    searchForCSum,
} TFpgaParserState;

TFpgaParserState fpgaParserState = searchForKey;

uint32_t data;
#define DATABUF_LEN 65535
uint32_t dataBuf[DATABUF_LEN], dataInd=0;


uint8_t curInd=0;
uint32_t unexpectedData=0;
uint32_t dataCounter = 0;

uint8_t keyConst[] = {0xee,0xee,0xee,
                      0x22,0x11,0xee,
                      0x44,0x33,0xee};
uint8_t keyBuf[9];
void processData(uint32_t d);
void processBuffer(uint8_t *b, uint16_t bytesReaded)
{
    for(int i=0; i<bytesReaded; i++){
        uint8_t d = b[i];
        switch(fpgaParserState){
        case searchForKey:
                for(int k=0; k<8; k++){
                    keyBuf[k] = keyBuf[k+1];
                }
                keyBuf[8] = d;

                if (memcmp(keyBuf, keyConst, 9) == 0){
                    fpgaParserState = searchForFrameHeaderFW;
                    curInd=0;
                }
                else{
                    unexpectedData++;
                }
                //if(d==0xee)
                    //fpgaParserState = searchForKeyEE2;

                break;
        default:
                data = (data>>8)|(d<<16);
                curInd++;
                if(curInd>2){
                    curInd=0;
                    processData(data);
                    data=0;
                }


                break;
        }
    }
}

void processData(uint32_t d)
{
    if((d==0xeeeeee) || (d==0xee1122)){
     //   fpgaParserState = searchForKey;
    }

    switch(fpgaParserState){
    case searchForFrameHeaderFW:
        if((d&0x3E000) == 0x30000){
            fpgaParserState = searchForFrameHeaderSW;
        }
        break;
    case searchForFrameHeaderSW:
        if((d&0x3E000) == 0x32000){

        }
        break;
    case searchForPacketHeaderFW:
        if((d&0x3E000) == 0x34000){

        }
        break;
    case searchForPacketHeaderSW:
        if((d&0x3E000) == 0x36000){

        }
        break;
    case searchForPacketEpilogFW:
        if((d&0x3E000)==0x3C000){

        }
        break;
    case searchForPacketEpilogSW:
        if((d&0x3E000)==0x3E000){

        }
        break;
    case searchForFrameEpilogFW:
        if((d&0x3E000)==0x38000){

        }
        break;
    case searchForFrameEpilogSW:
        if((d&0x3E000)==0x3A000){

        }
        break;
    }
}

uint32_t bToSend=0;
uint8_t bufToSend[65535];
void sendData(char *dataBuf, uint32_t bufLen)
{
    memcpy(bufToSend, dataBuf, bufLen);
    bToSend = bufLen;
    printf("sendData ptr:%x, bLen:%d", dataBuf, bufLen);
}

void threadUsb(void *arg)
{
    FT_HANDLE ftHandle;
    FT_STATUS ftStatus;
    DWORD dwDriverVer;
    DWORD dwLibraryVer;
    DWORD rxBytes, txBytes;
    DWORD evStatus;
    HANDLE hEvent;
    DWORD EventMask;
    //DWORD threadID;

    //cout << "task1 says: " << msg;

    ftStatus = FT_GetLibraryVersion(&dwLibraryVer);
    if (ftStatus == FT_OK)
        printf("Library version = 0x%x\n",dwLibraryVer);
    else
        printf("error reading library version\n");


    ftStatus = FT_Open(0,&ftHandle);
    if (ftStatus != FT_OK) {
        printf("FT_Open failed\n");
        _endthreadex(-1);

    }

    ftStatus = FT_GetDriverVersion(ftHandle,&dwDriverVer);
    if (ftStatus != FT_OK){
        printf("error reading driver version\n");
        _endthreadex(-1);
    }
    printf("Driver version = 0x%x\n",dwDriverVer);

    ftStatus = FT_SetBitMode(ftHandle, 0xff, 0x00);
    if (ftStatus != FT_OK){
        printf("error reading driver version\n");
        _endthreadex(-1);
    }

    ftStatus = FT_SetBitMode(ftHandle, 0xff, 0x40);
    if (ftStatus != FT_OK){
        printf("error reading driver version\n");
        _endthreadex(-1);
    }

    hEvent = CreateEvent(
    NULL,
    false, // auto-reset event
    false, // non-signalled state
    ""
    );
    EventMask = FT_EVENT_RXCHAR;
    ftStatus = FT_SetEventNotification(ftHandle,EventMask,hEvent);


    uint32_t iter=0, wr=0, wor=0;

    DWORD lastTickCount=GetTickCount();
    uint32_t bytesRecvd=0;
    for(;bRunning==true;iter++){

        if((GetTickCount()-lastTickCount)>1000){
            lastTickCount = GetTickCount();
            printf("iters %d, wr %d, wor %d, recvd %.3f KB unexpected %d dc:%d\n", iter, wr, wor, bytesRecvd/1024., unexpectedData, dataCounter);
            iter=0; wr=0; wor=0;
            bytesRecvd = 0;
        }

        WaitForSingleObject(hEvent, 50);

        ftStatus = FT_GetStatus(ftHandle, &rxBytes, &txBytes, &evStatus);
        if (ftStatus != FT_OK){
            printf("FT_GetStatus error\n");
            //_endthreadex(-1);
            break;
        }
        //if((rxBytes>0) || (evStatus!=0))
        //    printf("rxBytes %d, txBytes %d, evStatus %x\n", rxBytes, txBytes, evStatus);

        ftStatus =  FT_GetQueueStatus(ftHandle, &rxBytes);
        if (ftStatus != FT_OK){
            printf("FT_GetQueueStatus error\n");
            //_endthreadex(-1);
            break;
        }
        uint16_t bytesReaded=0;
        if(rxBytes>0){
            wr++;
            //memset(&(recvBuf[0]), 0, 65536*sizeof(uint8_t));
            ftStatus = FT_Read(ftHandle, &(recvBuf[0]), rxBytes, (DWORD*)&bytesReaded);
            bytesRecvd += bytesReaded;
            processBuffer(&(recvBuf[0]), bytesReaded);
        }
        else
            wor++;
        //if(rxBytes>0)
          //  printf("rxBytes %d\n", rxBytes);
        if(bToSend > 0){

        }
    }

    FT_Close(ftHandle);
    _endthreadex(0);
}

