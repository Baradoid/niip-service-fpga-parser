TARGET = fpgaParser
TEMPLATE = lib
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(debug, debug|release) {
    DEBUG_OR_RELEASE = debug
}  else {
    DEBUG_OR_RELEASE = release
}

CONFIG(debug,debug|release) DESTDIR = debug
CONFIG(release,debug|release) DESTDIR = release

QMAKE_POST_LINK += "copy $${DESTDIR}\\$${TARGET}.dll _fpgaParser.pyd"

QMAKE_LFLAGS += -static-libgcc -static-libstdc++
QMAKE_CXXFLAGS_RELEASE += -static-libgcc -static-libstdc++

SOURCES += \
   fpgaParser.c \
   fpgaParser_wrap.c

INCLUDEPATH += $$PWD\ft
#INCLUDEPATH += C:\Python37-32\include
#LIBS += -L$$PWD\ft\i386\ftd2xx.lib
LIBS += -L$$PWD\ft\i386 -lftd2xx

#LIBS += -LC:\Python37-32\libs  -lpython37
INCLUDEPATH += D:\Python\Python38-32\include
LIBS += D:\Python\Python38-32\libs\python38.lib

HEADERS += \
   fpgaParser.h
